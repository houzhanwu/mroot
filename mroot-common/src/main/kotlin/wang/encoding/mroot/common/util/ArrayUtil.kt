/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util


import java.math.BigInteger

/**
 * 数组工具类
 * @author ErYang
 */
object ArrayUtil {

    /**
     * 逗号
     */
    private const val COMMA_NAME: String = ","

    /**
     * String 数组 转换成 BigInteger 数组
     *
     * @param array String 数组
     * @return BigInteger 数组
     */
    fun string2BigInteger(array: Array<String>): ArrayList<BigInteger>? {
        val idArray: ArrayList<BigInteger> = arrayListOf()
        var id: BigInteger?
        for (i: Int in array.indices) {
            id = array[i].toBigIntegerOrNull()
            if (null != id && BigInteger.ZERO < id) {
                idArray.add(id)
            }
        }
        return idArray
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * String 数组 转换成 BigInteger 数组
     *
     * @param array String 数组
     * @return BigInteger 数组
     */
    fun string2BigIntegerArray(array: Array<String>): Array<BigInteger>? {
        val idArray: Array<BigInteger> = Array(array.size,{BigInteger.ZERO })
        var id: BigInteger?
        for (i: Int in array.indices) {
            id = array[i].toBigIntegerOrNull()
            if (null != id && BigInteger.ZERO < id) {
                idArray[i]=id
            }
        }
        return idArray
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * String 转换成 BigInteger 数组
     *
     * @param str String
     * @return BigInteger 数组
     */
    fun string2BigInteger(str: String): Array<BigInteger>? {

        val strAry: List<String> = str.split(COMMA_NAME)
        val idArray: Array<BigInteger> = Array(strAry.size,{BigInteger.ZERO })
        var id: BigInteger?
        for ((i: Int, strValue:String) in strAry.withIndex()) {
            id = strValue.toBigIntegerOrNull()
            if (null != id && BigInteger.ZERO < id) {
                idArray[i] = id
            }
        }
        return idArray
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ArrayUtil class

/* End of file ArrayUtil.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/common/util/ArrayUtil.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
